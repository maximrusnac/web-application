package com.endava.intership.webapplication.repository.hibernate;

import com.endava.intership.webapplication.WebApplication;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Location;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = WebApplication.class)
@ActiveProfiles({"test", "hibernate"})
class DepartmentRepositoryHibernateImplTest {

    @Autowired
    private DepartmentRepositoryHibernateImpl departmentRepository;

    private final int DEFAULT_DEPARTMENT_ID = 80;

    private final Employee MANAGER = Employee.builder().employeeId(100).build();

    private final Location LOCATION = Location.builder().locationId(1700).build();

    private final Department DEPARTMENT = Department.builder()
            .departmentName("New department")
            .manager(MANAGER)
            .location(LOCATION)
            .build();

    @Test
    void testFindAll() {
        int generalAmountDepartments = 27;
        assertThat(departmentRepository.findAll().size()).isEqualTo(generalAmountDepartments);
    }

    @Test
    void testFindById() {
        String departmentName = "Sales";

        assertThat(departmentRepository.findById(DEFAULT_DEPARTMENT_ID).get().getDepartmentName()).isEqualTo(departmentName);
    }

    @Test
    @DirtiesContext
    void testSave() {
        departmentRepository.save(DEPARTMENT);

        assertThat(departmentRepository.findById(DEPARTMENT.getDepartmentId()).get().getDepartmentName()).isEqualTo(DEPARTMENT.getDepartmentName());
    }

    @Test
    @DirtiesContext
    void testUpdate() {
        DEPARTMENT.setDepartmentId(DEFAULT_DEPARTMENT_ID);
        departmentRepository.save(DEPARTMENT);

        assertThat(departmentRepository.findById(DEFAULT_DEPARTMENT_ID).get().getDepartmentName()).isEqualTo(DEPARTMENT.getDepartmentName());
    }

    @Test
    @DirtiesContext
    void testDeleteById() {
        departmentRepository.freeEmployeesFromDepartment(DEFAULT_DEPARTMENT_ID);
        departmentRepository.deleteById(DEFAULT_DEPARTMENT_ID);

        assertThat(departmentRepository.findById(DEFAULT_DEPARTMENT_ID)).isEqualTo(Optional.empty());

    }

    @Test
    @DirtiesContext
    void testFreeEmployeesFromDepartment() {
        departmentRepository.freeEmployeesFromDepartment(DEFAULT_DEPARTMENT_ID);
    }

}