package com.endava.intership.webapplication.repository.jdbc;

import com.endava.intership.webapplication.WebApplication;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import com.endava.intership.webapplication.repository.hibernate.EmployeeRepositoryHibernateImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = WebApplication.class)
@ActiveProfiles({"test", "jdbc"})
class EmployeeRepositoryImplTest {

    @Autowired
    private EmployeeRepositoryImpl employeeRepository;

    private final int EMPLOYEE_DEFAULT_ID = 105;

    private final Department DEPARTMENT = Department.builder().departmentId(100).build();

    private final Job JOB = Job.builder().jobId("ST_MAN").build();

    private final Date HIRE_DATE = Date.valueOf("2020-07-16");

    private final BigDecimal SALARY = new BigDecimal("2500.0");

    private final BigDecimal COMMISSION_PCT = new BigDecimal(0);

    private final Employee EMPLOYEE = Employee.builder()
            .firstName("Nickola")
            .lastName("Tesla")
            .email("Nickola@Tesla")
            .phoneNumber("012345678")
            .hireDate(HIRE_DATE)
            .job(JOB)
            .salary(SALARY)
            .commissionPct(COMMISSION_PCT)
            .department(DEPARTMENT)
            .manager(null)
            .build();

    @Test
    void testFindAll() {
        int generalAmountDepartments = 79;
        assertThat(employeeRepository.findAll().size()).isEqualTo(generalAmountDepartments);

    }

    @Test
    void testFindById() {
        String employeeName = "David";

        assertThat(employeeRepository.findById(EMPLOYEE_DEFAULT_ID).get().getFirstName()).isEqualTo(employeeName);

    }

    @Test
    @DirtiesContext
    void testSave() {
        employeeRepository.save(EMPLOYEE);

        assertThat(employeeRepository.findById(EMPLOYEE.getEmployeeId()).get().getFirstName()).isEqualTo(EMPLOYEE.getFirstName());
    }

    @Test
    @DirtiesContext
    void testUpdate() {
        EMPLOYEE.setEmployeeId(EMPLOYEE_DEFAULT_ID);
        employeeRepository.save(EMPLOYEE);

        assertThat(employeeRepository.findById(EMPLOYEE_DEFAULT_ID).get().getFirstName()).isEqualTo(EMPLOYEE.getFirstName());
    }

    @Test
    @DirtiesContext
    void testDeleteById() {
        employeeRepository.freeManager(EMPLOYEE_DEFAULT_ID);
        employeeRepository.freeDepartmentFromManager(EMPLOYEE_DEFAULT_ID);
        employeeRepository.deleteById(EMPLOYEE_DEFAULT_ID);

        assertThat(employeeRepository.findById(EMPLOYEE_DEFAULT_ID)).isEqualTo(Optional.empty());
    }

    @Test
    @DirtiesContext
    void testFreeManager() {
        employeeRepository.freeManager(EMPLOYEE_DEFAULT_ID);
    }

    @Test
    @DirtiesContext
    void testFreeDepartmentFromManager() {
        employeeRepository.freeDepartmentFromManager(EMPLOYEE_DEFAULT_ID);
    }
}