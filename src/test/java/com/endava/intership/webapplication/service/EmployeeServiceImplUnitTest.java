package com.endava.intership.webapplication.service;

import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoInputImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoLimitedFieldsImpl;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import com.endava.intership.webapplication.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplUnitTest {

    @Mock
    private EmployeeRepository employeeRepositoryMock;

    @Mock
    private ModelMapper modelMapperMock;

    @InjectMocks
    private EmployeeServiceImpl employeeServiceImpl;

    private final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final Job JOB = Job.builder().jobId("ST_MAN").build();

    private final Department DEPARTMENT = Department.builder().departmentId(80).build();

    private final Date HIRE_DATE = Date.valueOf("2020-07-16");

    private final BigDecimal SALARY = new BigDecimal("2500.0");

    private final BigDecimal COMMISSION_PCT = new BigDecimal(0);

    private final Integer ID_NOT_EXISTING_EMPLOYEE = 0;

    private final Employee EMPLOYEE = Employee.builder()
            .employeeId(1)
            .firstName("Nickola")
            .lastName("Tesla")
            .email("Nickola@Tesla")
            .phoneNumber("012345678")
            .hireDate(HIRE_DATE)
            .job(JOB)
            .salary(SALARY)
            .commissionPct(COMMISSION_PCT)
            .department(DEPARTMENT)
            .manager(null)
            .build();

    private final EmployeeDtoInputImpl EMPLOYEE_DTO_INPUT = MODEL_MAPPER.map(EMPLOYEE, EmployeeDtoInputImpl.class);

    private final EmployeeDtoAllFieldsImpl EMPLOYEE_ALL_FIELDS = MODEL_MAPPER.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class);

    private final EmployeeDtoLimitedFieldsImpl EMPLOYEE_LIMITED_FIELDS = MODEL_MAPPER.map(EMPLOYEE, EmployeeDtoLimitedFieldsImpl.class);


    @Test
    void testFindAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        List<EmployeeDto> employeesDtoAllFields = new ArrayList<>();
        List<EmployeeDto> employeesDtoLimitedFields = new ArrayList<>();
        employees.add(EMPLOYEE);
        employeesDtoAllFields.add(EMPLOYEE_ALL_FIELDS);
        employeesDtoLimitedFields.add(EMPLOYEE_LIMITED_FIELDS);

        when(employeeRepositoryMock.findAll()).thenReturn(employees);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class)).thenReturn(EMPLOYEE_ALL_FIELDS);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoLimitedFieldsImpl.class)).thenReturn(EMPLOYEE_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(employeeServiceImpl.findAllEmployees(true)).isEqualTo(employeesDtoAllFields),
                () -> assertThat(employeeServiceImpl.findAllEmployees(false)).isEqualTo(employeesDtoLimitedFields)
        );
    }

    @Test
    void testFindByIdEmployee() {
        when(employeeRepositoryMock.findById(EMPLOYEE.getEmployeeId())).thenReturn(Optional.of(EMPLOYEE));
        when(employeeRepositoryMock.findById(ID_NOT_EXISTING_EMPLOYEE)).thenReturn(Optional.empty());
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class)).thenReturn(EMPLOYEE_ALL_FIELDS);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoLimitedFieldsImpl.class)).thenReturn(EMPLOYEE_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(employeeServiceImpl.findByIdEmployee(EMPLOYEE.getEmployeeId(), true)).isEqualTo(EMPLOYEE_ALL_FIELDS),
                () -> assertThat(employeeServiceImpl.findByIdEmployee(EMPLOYEE.getEmployeeId(), false)).isEqualTo(EMPLOYEE_LIMITED_FIELDS),
                () -> assertThatThrownBy(() -> employeeServiceImpl.findByIdEmployee(ID_NOT_EXISTING_EMPLOYEE, true)).isInstanceOf(NoSuchElementException.class),
                () -> assertThatThrownBy(() -> employeeServiceImpl.findByIdEmployee(ID_NOT_EXISTING_EMPLOYEE, false)).isInstanceOf(NoSuchElementException.class)
        );
    }

    @Test
    void testSaveEmployee() {
        when(modelMapperMock.map(EMPLOYEE_DTO_INPUT, Employee.class)).thenReturn(EMPLOYEE);
        when(employeeRepositoryMock.save(EMPLOYEE)).thenReturn(EMPLOYEE);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class)).thenReturn(EMPLOYEE_ALL_FIELDS);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoLimitedFieldsImpl.class)).thenReturn(EMPLOYEE_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(employeeServiceImpl.saveEmployee(EMPLOYEE_DTO_INPUT, true)).isEqualTo(EMPLOYEE_ALL_FIELDS),
                () -> assertThat(employeeServiceImpl.saveEmployee(EMPLOYEE_DTO_INPUT, false)).isEqualTo(EMPLOYEE_LIMITED_FIELDS)
        );
    }

    @Test
    void testDeleteEmployee() {
        doNothing().when(employeeRepositoryMock).deleteById(anyInt());
        doNothing().when(employeeRepositoryMock).freeManager(anyInt());
        doNothing().when(employeeRepositoryMock).freeDepartmentFromManager(anyInt());

        assertDoesNotThrow(() -> employeeServiceImpl.deleteEmployee(ID_NOT_EXISTING_EMPLOYEE));
    }

    @Test
    void testUpdateEmployee() {
        when(employeeRepositoryMock.findById(EMPLOYEE.getEmployeeId())).thenReturn(Optional.of(EMPLOYEE));
        when(employeeRepositoryMock.findById(ID_NOT_EXISTING_EMPLOYEE)).thenReturn(Optional.empty());
        when(modelMapperMock.map(EMPLOYEE_DTO_INPUT, Employee.class)).thenReturn(EMPLOYEE);
        when(employeeRepositoryMock.save(EMPLOYEE)).thenReturn(EMPLOYEE);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class)).thenReturn(EMPLOYEE_ALL_FIELDS);
        when(modelMapperMock.map(EMPLOYEE, EmployeeDtoLimitedFieldsImpl.class)).thenReturn(EMPLOYEE_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(employeeServiceImpl.updateEmployee(EMPLOYEE.getEmployeeId(), EMPLOYEE_DTO_INPUT, true)).isEqualTo(EMPLOYEE_ALL_FIELDS),
                () -> assertThat(employeeServiceImpl.updateEmployee(EMPLOYEE.getEmployeeId(), EMPLOYEE_DTO_INPUT, false)).isEqualTo(EMPLOYEE_LIMITED_FIELDS),
                () -> assertThatThrownBy(() -> employeeServiceImpl.updateEmployee(ID_NOT_EXISTING_EMPLOYEE, EMPLOYEE_DTO_INPUT, true)).isInstanceOf(NoSuchElementException.class)
        );
    }
}