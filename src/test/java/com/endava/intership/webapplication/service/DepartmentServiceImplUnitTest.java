package com.endava.intership.webapplication.service;

import com.endava.intership.webapplication.model.DTO.department.DepartmentDto;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoInputImpl;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoLimitedFieldsImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoInputImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoLimitedFieldsImpl;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Location;
import com.endava.intership.webapplication.repository.DepartmentRepository;
import com.endava.intership.webapplication.repository.EmployeeRepository;
import com.endava.intership.webapplication.repository.jdbc.DepartmentRepositoryImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DepartmentServiceImplUnitTest {
    @Mock
    private DepartmentRepository departmentRepositoryMock;

    @Mock
    private ModelMapper modelMapperMock;

    @InjectMocks
    private DepartmentServiceImpl departmentServiceImpl;

    private final Integer ID_NOT_EXISTING_DEPARTMENT = 0;

    private final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final Employee MANAGER = Employee.builder().employeeId(100).build();

    private final Location LOCATION = Location.builder().locationId(1700).build();

    private final Department DEPARTMENT = Department.builder()
            .departmentId(1)
            .departmentName("New department")
            .manager(MANAGER)
            .location(LOCATION)
            .build();

    private final DepartmentDtoInputImpl DEPARTMENT_DTO_INPUT = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoInputImpl.class);

    private final DepartmentDtoAllFieldsImpl DEPARTMENT_ALL_FIELDS = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class);

    private final DepartmentDtoLimitedFieldsImpl DEPARTMENT_LIMITED_FIELDS = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoLimitedFieldsImpl.class);

    @Test
    void testFindAllDepartments() {
        List<Department> departments = new ArrayList<>();
        List<DepartmentDto> employeesDtoAllFields = new ArrayList<>();
        List<DepartmentDto> employeesDtoLimitedFields = new ArrayList<>();
        departments.add(DEPARTMENT);
        employeesDtoAllFields.add(DEPARTMENT_ALL_FIELDS);
        employeesDtoLimitedFields.add(DEPARTMENT_LIMITED_FIELDS);

        when(departmentRepositoryMock.findAll()).thenReturn(departments);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class)).thenReturn(DEPARTMENT_ALL_FIELDS);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoLimitedFieldsImpl.class)).thenReturn(DEPARTMENT_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(departmentServiceImpl.findAllDepartments(true)).isEqualTo(employeesDtoAllFields),
                () -> assertThat(departmentServiceImpl.findAllDepartments(false)).isEqualTo(employeesDtoLimitedFields)
        );
    }

    @Test
    void testFindByIdDepartment() {
        when(departmentRepositoryMock.findById(DEPARTMENT.getDepartmentId())).thenReturn(Optional.of(DEPARTMENT));
        when(departmentRepositoryMock.findById(ID_NOT_EXISTING_DEPARTMENT)).thenReturn(Optional.empty());
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class)).thenReturn(DEPARTMENT_ALL_FIELDS);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoLimitedFieldsImpl.class)).thenReturn(DEPARTMENT_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(departmentServiceImpl.findByIdDepartment(DEPARTMENT.getDepartmentId(), true)).isEqualTo(DEPARTMENT_ALL_FIELDS),
                () -> assertThat(departmentServiceImpl.findByIdDepartment(DEPARTMENT.getDepartmentId(), false)).isEqualTo(DEPARTMENT_LIMITED_FIELDS),
                () -> assertThatThrownBy(() -> departmentServiceImpl.findByIdDepartment(ID_NOT_EXISTING_DEPARTMENT, true)).isInstanceOf(NoSuchElementException.class),
                () -> assertThatThrownBy(() -> departmentServiceImpl.findByIdDepartment(ID_NOT_EXISTING_DEPARTMENT, false)).isInstanceOf(NoSuchElementException.class)
        );
    }

    @Test
    void testSaveDepartment() {
        when(modelMapperMock.map(DEPARTMENT_DTO_INPUT, Department.class)).thenReturn(DEPARTMENT);
        when(departmentRepositoryMock.save(DEPARTMENT)).thenReturn(DEPARTMENT);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class)).thenReturn(DEPARTMENT_ALL_FIELDS);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoLimitedFieldsImpl.class)).thenReturn(DEPARTMENT_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(departmentServiceImpl.saveDepartment(DEPARTMENT_DTO_INPUT, true)).isEqualTo(DEPARTMENT_ALL_FIELDS),
                () -> assertThat(departmentServiceImpl.saveDepartment(DEPARTMENT_DTO_INPUT, false)).isEqualTo(DEPARTMENT_LIMITED_FIELDS)
        );
    }

    @Test
    void testDeleteDepartment() {
        doNothing().when(departmentRepositoryMock).deleteById(anyInt());
        doNothing().when(departmentRepositoryMock).freeEmployeesFromDepartment(anyInt());

        assertDoesNotThrow(() -> departmentServiceImpl.deleteDepartment(ID_NOT_EXISTING_DEPARTMENT));
    }

    @Test
    void testUpdateDepartment() {
        when(departmentRepositoryMock.findById(DEPARTMENT.getDepartmentId())).thenReturn(Optional.of(DEPARTMENT));
        when(departmentRepositoryMock.findById(ID_NOT_EXISTING_DEPARTMENT)).thenReturn(Optional.empty());
        when(modelMapperMock.map(DEPARTMENT_DTO_INPUT, Department.class)).thenReturn(DEPARTMENT);
        when(departmentRepositoryMock.save(DEPARTMENT)).thenReturn(DEPARTMENT);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class)).thenReturn(DEPARTMENT_ALL_FIELDS);
        when(modelMapperMock.map(DEPARTMENT, DepartmentDtoLimitedFieldsImpl.class)).thenReturn(DEPARTMENT_LIMITED_FIELDS);

        assertAll(
                () -> assertThat(departmentServiceImpl.updateDepartment(DEPARTMENT.getDepartmentId(), DEPARTMENT_DTO_INPUT, true)).isEqualTo(DEPARTMENT_ALL_FIELDS),
                () -> assertThat(departmentServiceImpl.updateDepartment(DEPARTMENT.getDepartmentId(), DEPARTMENT_DTO_INPUT, false)).isEqualTo(DEPARTMENT_LIMITED_FIELDS),
                () -> assertThatThrownBy(() -> departmentServiceImpl.updateDepartment(ID_NOT_EXISTING_DEPARTMENT, DEPARTMENT_DTO_INPUT, true)).isInstanceOf(NoSuchElementException.class)
        );
    }
}