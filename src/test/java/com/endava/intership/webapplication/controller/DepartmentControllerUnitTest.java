package com.endava.intership.webapplication.controller;

import com.endava.intership.webapplication.model.DTO.department.DepartmentDto;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoInputImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import com.endava.intership.webapplication.model.entity.Location;
import com.endava.intership.webapplication.service.DepartmentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DepartmentController.class)
class DepartmentControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentServiceImpl;

    private final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final Employee MANAGER = Employee.builder().employeeId(100).build();

    private final Location LOCATION = Location.builder().locationId(1700).build();

    private final Department DEPARTMENT = Department.builder()
            .departmentId(1)
            .departmentName("New department")
            .manager(MANAGER)
            .location(LOCATION)
            .build();

    @Test
    void testFindAllDepartments() throws Exception {

        List<DepartmentDto> departments = new ArrayList<>();
        departments.add(MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class));

        when(departmentServiceImpl.findAllDepartments(true)).thenReturn(departments);

        this.mockMvc.perform(get("/api/departments/?expand=true")).andExpect(status().isFound())
                .andExpect(jsonPath("$[*].departmentId").value(1));
    }

    @Test
    void testFindByIdDepartment() throws Exception {

        DepartmentDtoAllFieldsImpl departmentDtoAllFields = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class);

        when(departmentServiceImpl.findByIdDepartment(1, true)).thenReturn(departmentDtoAllFields);
        when(departmentServiceImpl.findByIdDepartment(0, true)).thenThrow(NoSuchElementException.class);

        this.mockMvc.perform(get("/api/departments/1?expand=true")).andExpect(status().isFound());
        this.mockMvc.perform(get("/api/departments/0?expand=true")).andExpect(status().isBadRequest());
    }

    @Test
    void testSaveDepartment() throws Exception {
        DepartmentDtoInputImpl departmentDtoInput = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoInputImpl.class);
        DepartmentDtoAllFieldsImpl departmentDtoAllFields = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class);

        when(departmentServiceImpl.saveDepartment(departmentDtoInput, true)).thenReturn(departmentDtoAllFields);

        this.mockMvc.perform(post("/api/departments/?expand=true")
                .content(OBJECT_MAPPER.writeValueAsString(departmentDtoInput))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());
    }

    @Test
    void saveUpdateDepartment() throws Exception {
        DepartmentDtoInputImpl departmentDtoInput = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoInputImpl.class);
        DepartmentDtoAllFieldsImpl departmentDtoAllFields = MODEL_MAPPER.map(DEPARTMENT, DepartmentDtoAllFieldsImpl.class);

        when(departmentServiceImpl.updateDepartment(1, departmentDtoInput, true)).thenReturn(departmentDtoAllFields);
        when(departmentServiceImpl.updateDepartment(0, departmentDtoInput, true)).thenThrow(NoSuchElementException.class);

        this.mockMvc.perform(put("/api/departments/1?expand=true")
                .content(OBJECT_MAPPER.writeValueAsString(departmentDtoInput))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());

        this.mockMvc.perform(put("/api/departments/0?expand=true")
                .content(OBJECT_MAPPER.writeValueAsString(departmentDtoInput))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    void saveDeleteDepartment() throws Exception {
        doNothing().when(departmentServiceImpl).deleteDepartment(1);

        this.mockMvc.perform(delete("/api/departments/1?expand=true")).andExpect(status().isNoContent());

    }
}