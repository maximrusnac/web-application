package com.endava.intership.webapplication.controller;

import com.endava.intership.webapplication.WebApplication;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertAll;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test", "hibernate"})
class EmployeeControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    private final static Job JOB = Job.builder().jobId("ST_MAN").build();

    private final static Department DEPARTMENT = Department.builder().departmentId(80).build();

    private final static Date HIRE_DATE = Date.valueOf("2020-07-16");

    private final static BigDecimal SALARY = new BigDecimal("2500.0");

    private final static BigDecimal COMMISSION_PCT = new BigDecimal(0);

    private final static Employee EMPLOYEE = Employee.builder()
            .firstName("Nickola")
            .lastName("Tesla")
            .email("Nickola@Tesla")
            .phoneNumber("012345678")
            .hireDate(HIRE_DATE)
            .job(JOB)
            .salary(SALARY)
            .commissionPct(COMMISSION_PCT)
            .department(DEPARTMENT)
            .manager(null)
            .build();

    @Test
    void testFindAllEmployees() {
        RestTemplate restTemplate = new RestTemplate();
        String resourceUrl = createURLWithPort("/api/employees/?expand=true");
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FOUND);
    }

    @Test
    void testFindByIdEmployee() {
        RestTemplate restTemplate = new RestTemplate();
        String resourceUrl = createURLWithPort("/api/employees/100?expand=true");
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);

        assertAll(
                () -> assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FOUND),
                () -> assertThat(response.getBody()).isEqualTo("{\"employeeId\":100,\"firstName\":\"Steven\",\"lastName\":\"King\",\"email\":\"SKING\",\"phoneNumber\":\"515.123.4567\",\"hireDate\":\"2003-06-17\",\"jobId\":\"AD_PRES\",\"salary\":24000.0,\"commissionPct\":null,\"managerId\":100,\"departmentId\":90}")
        );
    }

    @ParameterizedTest
    @MethodSource("provideEmployees")
    void testSaveEmployee(Employee employee, HttpStatus httpStatus) {
        String resourceUrl = createURLWithPort("/api/employees/?expand=true");

        HttpEntity<Employee> request = new HttpEntity<>(employee);
        ResponseEntity<Employee> response = restTemplate.exchange(resourceUrl, HttpMethod.POST, request, Employee.class);

        assertThat(response.getStatusCode()).isEqualTo(httpStatus);
    }

    @Test
    void testUpdateEmployee() {
        String resourceUrl = createURLWithPort("/api/employees/100?expand=true");
        Employee updatedEmployee = Employee.from(EMPLOYEE);

        updatedEmployee.setFirstName("Steven");
        updatedEmployee.setLastName("King");
        HttpEntity<Employee> request = new HttpEntity<>(updatedEmployee);
        EmployeeDtoAllFieldsImpl employeeAfterResponse = restTemplate.exchange(resourceUrl, HttpMethod.PUT, request, EmployeeDtoAllFieldsImpl.class).getBody();

        assertAll(
                () -> assertThat(employeeAfterResponse.getEmployeeId()).isEqualTo(100),
                () -> assertThat(employeeAfterResponse.getFirstName()).isEqualTo("Steven"),
                () -> assertThat(employeeAfterResponse.getLastName()).isEqualTo("King"),
                () -> assertThat(employeeAfterResponse.getPhoneNumber()).isEqualTo("012345678"),
                () -> assertThat(employeeAfterResponse.getSalary()).isEqualTo(2500)
        );
    }

    @Test
    void testDeleteEmployee() {
        String resourceUrl = createURLWithPort("/api/employees/102?expand=true");
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.delete(resourceUrl);

        assertThatThrownBy(() -> {
            restTemplate.getForEntity(resourceUrl, String.class);
        }).hasMessageContaining("Database does not contain such element");
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    private static Stream<Arguments> provideEmployees() {
        Employee employeeValidateNameBlankError = Employee.from(EMPLOYEE);
        Employee employeeValidateNameNullError = Employee.from(EMPLOYEE);
        Employee employeeValidatePhoneNumberError = Employee.from(EMPLOYEE);
        Employee employeeValidateEmailError = Employee.from(EMPLOYEE);
        Employee employeeValidateSalaryError = Employee.from(EMPLOYEE);
        BigDecimal salary = new BigDecimal(0);

        employeeValidateNameNullError.setFirstName(null);
        employeeValidateNameBlankError.setFirstName(" ");
        employeeValidatePhoneNumberError.setPhoneNumber("12354789");
        employeeValidateEmailError.setEmail("email");
        employeeValidateSalaryError.setSalary(salary);

        return Stream.of(
                Arguments.of(EMPLOYEE, HttpStatus.CREATED),
                Arguments.of(employeeValidateNameNullError, HttpStatus.BAD_REQUEST),
                Arguments.of(employeeValidateNameBlankError, HttpStatus.BAD_REQUEST),
                Arguments.of(employeeValidatePhoneNumberError, HttpStatus.BAD_REQUEST),
                Arguments.of(employeeValidateEmailError, HttpStatus.BAD_REQUEST),
                Arguments.of(employeeValidateSalaryError, HttpStatus.BAD_REQUEST)
        );

    }
}