package com.endava.intership.webapplication.controller;

import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoInputImpl;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import com.endava.intership.webapplication.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
class EmployeeControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeServiceImpl;

    private final ModelMapper modelMapper = new ModelMapper();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Department DEPARTMENT = Department.builder().departmentId(80).build();

    private final Job JOB = Job.builder().jobId("ST_MAN").build();

    private final Date HIRE_DATE = Date.valueOf("2020-07-16");

    private final BigDecimal SALARY = new BigDecimal("2500.0");

    private final BigDecimal COMMISSION_PCT = new BigDecimal(0);

    private final Employee EMPLOYEE = Employee.builder()
            .employeeId(1)
            .firstName("Nickola")
            .lastName("Tesla")
            .email("Nickola@Tesla")
            .phoneNumber("012345678")
            .hireDate(HIRE_DATE)
            .job(JOB)
            .salary(SALARY)
            .commissionPct(COMMISSION_PCT)
            .department(DEPARTMENT)
            .manager(null)
            .build();


    @Test
    void testFindAllEmployees() throws Exception {
        List<EmployeeDto> employees = new ArrayList<>();
        employees.add(modelMapper.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class));

        when(employeeServiceImpl.findAllEmployees(true)).thenReturn(employees);

        this.mockMvc.perform(get("/api/employees/?expand=true")).andExpect(status().isFound())
                .andExpect(jsonPath("$[*].employeeId").value(1));
    }

    @Test
    void testFindByIdEmployee() throws Exception {
        EmployeeDtoAllFieldsImpl employeeAfterMapping = modelMapper.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class);

        when(employeeServiceImpl.findByIdEmployee(1, true)).thenReturn(employeeAfterMapping);
        when(employeeServiceImpl.findByIdEmployee(0, true)).thenThrow(NoSuchElementException.class);

        this.mockMvc.perform(get("/api/employees/1?expand=true")).andExpect(status().isFound());
        this.mockMvc.perform(get("/api/employees/0?expand=true")).andExpect(status().isBadRequest());
    }

    @Test
    void testSaveEmployee() throws Exception {
        EmployeeDtoInputImpl employeeDtoInput = modelMapper.map(EMPLOYEE, EmployeeDtoInputImpl.class);
        EmployeeDtoAllFieldsImpl employeeDtoAllFields = modelMapper.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class);
        employeeDtoAllFields.setEmployeeId(1);

        when(employeeServiceImpl.saveEmployee(employeeDtoInput, true)).thenReturn(employeeDtoAllFields);

        this.mockMvc.perform(post("/api/employees/?expand=true")
                .content(objectMapper.writeValueAsString(employeeDtoInput))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());
    }

    @Test
    void testUpdateEmployee() throws Exception {
        EmployeeDtoInputImpl employeeDtoInput = modelMapper.map(EMPLOYEE, EmployeeDtoInputImpl.class);
        EmployeeDtoAllFieldsImpl employeeDtoAllFields = modelMapper.map(EMPLOYEE, EmployeeDtoAllFieldsImpl.class);
        employeeDtoAllFields.setEmployeeId(1);

        when(employeeServiceImpl.updateEmployee(1, employeeDtoInput, true)).thenReturn(employeeDtoAllFields);
        when(employeeServiceImpl.updateEmployee(0, employeeDtoInput, true)).thenThrow(NoSuchElementException.class);

        this.mockMvc.perform(put("/api/employees/1?expand=true")
                .content(objectMapper.writeValueAsString(employeeDtoInput))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());

        this.mockMvc.perform(put("/api/employees/0?expand=true")
                .content(objectMapper.writeValueAsString(employeeDtoInput))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteEmployee() throws Exception {
        doNothing().when(employeeServiceImpl).deleteEmployee(1);

        this.mockMvc.perform(delete("/api/employees/1?expand=true")).andExpect(status().isNoContent());
    }
}