package com.endava.intership.webapplication.exception;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

class ExceptionResponseUnitTest {

    private final LocalDate ERROR_DATE = LocalDate.now();
    private final String ERROR_CODE = org.springframework.http.HttpStatus.BAD_REQUEST + ": Database does not contain such field";
    private final String ERROR_MESSAGE = "ERROR MESSAGE";
    private ExceptionResponse exceptionResponse;

    @BeforeEach
    void setUp() {
        exceptionResponse = new ExceptionResponse(ERROR_DATE, ERROR_CODE, ERROR_MESSAGE);
    }

    @Test
    void testGetErrorDate() {
        assertThat(exceptionResponse.getErrorDate()).isEqualTo(ERROR_DATE);
    }

    @Test
    void testGetErrorCode() {
        assertThat(exceptionResponse.getErrorCode()).isEqualTo(ERROR_CODE);
    }

    @Test
    void testGetMessage() {
        assertThat(exceptionResponse.getMessage()).isEqualTo(ERROR_MESSAGE);
    }

    @Test
    void testSetErrorDate() {
        LocalDate newErrorDate = LocalDate.of(2012, 11, 11);
        exceptionResponse.setErrorDate(newErrorDate);
        assertThat(exceptionResponse.getErrorDate()).isEqualTo(newErrorDate);
    }

    @Test
    void testSetErrorCode() {
        String newErrorCode = org.springframework.http.HttpStatus.BAD_REQUEST + ": New Error code";
        exceptionResponse.setErrorCode(newErrorCode);
        assertThat(exceptionResponse.getErrorCode()).isEqualTo(newErrorCode);
    }

    @Test
    void testSetMessage() {
        String newMessage = "Some message";
        exceptionResponse.setMessage(newMessage);
        assertThat(exceptionResponse.getMessage()).isEqualTo(newMessage);
    }

    @Test
    void testEquals() {
        ExceptionResponse exceptionResponseCopy = new ExceptionResponse(ERROR_DATE, ERROR_CODE, ERROR_MESSAGE);

        assertAll(
                () -> assertThat(exceptionResponse.equals(exceptionResponseCopy)).isTrue(),
                () -> assertThat(exceptionResponseCopy.equals(exceptionResponse)).isTrue()
        );

    }

    @Test
    void testCanEqual() {
        ExceptionResponse exceptionResponseCopy = new ExceptionResponse(ERROR_DATE, ERROR_CODE, ERROR_MESSAGE);
        String blankString = "";
        assertAll(
                () -> assertThat(exceptionResponse.canEqual(exceptionResponseCopy)).isTrue(),
                () -> assertThat(exceptionResponse.canEqual(blankString)).isFalse()
        );

    }

    @Test
    void testHashCode() {
        ExceptionResponse exceptionResponseCopy = new ExceptionResponse(ERROR_DATE, ERROR_CODE, ERROR_MESSAGE);
        assertThat(exceptionResponse.hashCode()).isEqualTo(exceptionResponseCopy.hashCode());
    }

    @Test
    void testToString() {
        assertThat(exceptionResponse.toString()).isEqualTo("ExceptionResponse(errorDate=" + ERROR_DATE + ", errorCode=" + ERROR_CODE + ", message=" + ERROR_MESSAGE + ")");
    }
}