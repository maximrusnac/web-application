package com.endava.intership.webapplication.exception;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.sql.SQLException;
import java.util.NoSuchElementException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ExceptionHandlerUnitTest {
    private final ExceptionHandler EXCEPTION_HANDLER = new ExceptionHandler();

    @Test
    void testHandleNoSuchFieldExceptions() {
        NoSuchFieldException noSuchFieldException = new NoSuchFieldException();

        assertThat(EXCEPTION_HANDLER.handleNoSuchFieldExceptions(noSuchFieldException).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void testHandleSqlExceptions() {
        SQLException sqlException = new SQLException();

        assertThat(EXCEPTION_HANDLER.handleSqlExceptions(sqlException).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

    @Test
    void testHandleNoSuchElementExceptions() {
        NoSuchElementException noSuchElementException = new NoSuchElementException();

        assertThat(EXCEPTION_HANDLER.handleNoSuchElementExceptions(noSuchElementException).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

}