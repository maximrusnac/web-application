alter table job_history
drop constraint JHIST_DEPT_FK;

alter table job_history
drop constraint JHIST_EMP_FK;

alter table job_history
drop constraint JHIST_JOB_FK;