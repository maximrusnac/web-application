package com.endava.intership.webapplication.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class ExceptionResponse {
    private LocalDate errorDate;
    private String errorCode;
    private String message;
}
