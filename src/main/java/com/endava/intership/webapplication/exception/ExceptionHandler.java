package com.endava.intership.webapplication.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.NoSuchElementException;

@Slf4j
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(NoSuchFieldException.class)
    public ResponseEntity<ExceptionResponse> handleNoSuchFieldExceptions(final NoSuchFieldException e) {
        log.error(e.getMessage(), e);
        return new ResponseEntity<>(
                new ExceptionResponse(LocalDate.now(), HttpStatus.BAD_REQUEST + ": Database does not contain such field", e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(SQLException.class)
    public ResponseEntity<ExceptionResponse> handleSqlExceptions(final SQLException e) {
        log.error(e.getMessage(), e);
        return new ResponseEntity<>(
                new ExceptionResponse(LocalDate.now(), HttpStatus.BAD_REQUEST + ": SQL server error", e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ExceptionResponse> handleNoSuchElementExceptions(final NoSuchElementException e) {
        log.error(e.getMessage(), e);
        return new ResponseEntity<>(
                new ExceptionResponse(LocalDate.now(), HttpStatus.BAD_REQUEST + ": Database does not contain such element", e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(
                new ExceptionResponse(LocalDate.now(), HttpStatus.BAD_REQUEST + ": Validation error", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
