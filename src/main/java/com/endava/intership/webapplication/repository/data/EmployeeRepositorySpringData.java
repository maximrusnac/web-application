package com.endava.intership.webapplication.repository.data;

import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.repository.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

@Profile("data")
public interface EmployeeRepositorySpringData extends JpaRepository<Employee, Integer>, EmployeeRepository {
    @Modifying
    @Query(value = "UPDATE employees SET manager_id = NULL WHERE manager_id = ?1",
            nativeQuery = true)
    void freeManager(Integer managerId);

    @Modifying
    @Query(value = " UPDATE departments SET manager_id = NULL WHERE manager_id = ?1",
            nativeQuery = true)
    void freeDepartmentFromManager(Integer managerId);
}