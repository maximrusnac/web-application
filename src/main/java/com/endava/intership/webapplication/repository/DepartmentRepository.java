package com.endava.intership.webapplication.repository;

import com.endava.intership.webapplication.model.entity.Department;

import java.util.List;
import java.util.Optional;

public interface DepartmentRepository {
    List<Department> findAll();

    Optional<Department> findById(Integer id);

    Department save(Department department);

    void deleteById(Integer id);

    void freeEmployeesFromDepartment(Integer id);

}
