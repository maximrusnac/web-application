package com.endava.intership.webapplication.repository.hibernate;

import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Profile("hibernate")
@RequiredArgsConstructor
@Repository
public class EmployeeRepositoryHibernateImpl implements EmployeeRepository {
    private final SessionFactory sessionFactory;

    @Override
    public List<Employee> findAll() {
        List<Employee> employees = null;
        try (Session session = sessionFactory.openSession()) {
            employees = session.createQuery("from Employee", Employee.class).list();
        }

        return employees;
    }

    @Override
    public Optional<Employee> findById(Integer id) {
        Employee employee = null;
        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("from Employee WHERE employeeId = :id", Employee.class);
            query.setParameter("id", id);
            query.setMaxResults(1);
            employee = query.uniqueResult();
        }

        return Optional.ofNullable(employee);
    }

    @Override
    public Employee save(Employee employee) {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(employee);
            transaction.commit();
        }
        return employee;
    }


    @Override
    public void deleteById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query query = session.createQuery("delete Employee where employeeId = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        }
    }

    @Override
    public void freeManager(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query query = session.createSQLQuery("UPDATE employees  SET manager_id = NULL WHERE manager_id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        }
    }


    @Override
    public void freeDepartmentFromManager(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query query = session.createSQLQuery("UPDATE departments  SET manager_id =NULL WHERE manager_id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        }
    }
}
