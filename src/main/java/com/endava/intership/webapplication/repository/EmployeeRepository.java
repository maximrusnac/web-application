package com.endava.intership.webapplication.repository;

import com.endava.intership.webapplication.model.entity.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {
    List<Employee> findAll();

    Optional<Employee> findById(Integer id);

    Employee save(Employee employee);

    void deleteById(Integer id);

    void freeManager(Integer id);

    void freeDepartmentFromManager(Integer id);


}
