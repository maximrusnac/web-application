package com.endava.intership.webapplication.repository.jdbc;

import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import com.endava.intership.webapplication.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Profile("jdbc")
@RequiredArgsConstructor
@Repository
@Slf4j
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final DataSource dataSource;

    @Override
    public List<Employee> findAll() {
        String sql = "SELECT * FROM employees";
        ArrayList<Employee> employees = null;
        try {
            try (Connection conn = dataSource.getConnection(); Statement stmt = conn.createStatement()) {

                stmt.execute(sql);

                ResultSet resultSet = stmt.getResultSet();
                employees = new ArrayList<>();
                while (resultSet.next()) {

                    log.error(String.valueOf(resultSet.getString("job_id")));
                    Job job = Job.builder().jobId(resultSet.getString("job_id")).build();
                    Employee manager = Employee.builder().employeeId(resultSet.getInt("manager_id") == 0 ? null : resultSet.getInt("manager_id")).build();
                    Department department = Department.builder().departmentId(resultSet.getInt("department_id") == 0 ? null : resultSet.getInt("department_id")).build();
                    Employee employee = Employee.builder()
                            .employeeId(resultSet.getInt("employee_id"))
                            .firstName(resultSet.getString("first_name"))
                            .lastName(resultSet.getString("last_name"))
                            .email(resultSet.getString("email"))
                            .phoneNumber(resultSet.getString("phone_number"))
                            .hireDate(resultSet.getDate("hire_date"))
                            .job(job)
                            .salary(resultSet.getBigDecimal("salary"))
                            .commissionPct(resultSet.getBigDecimal("commission_pct"))
                            .manager(manager)
                            .department(department)
                            .build();

                    employees.add(employee);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return employees;
    }

    @Override
    public Optional<Employee> findById(Integer id) {
        Employee employee = null;
        String sql = " SELECT * FROM employees WHERE employee_id = ? ";
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {

                stmt.setInt(1, id);
                stmt.execute();

                ResultSet resultSet = stmt.getResultSet();
                if (resultSet.next()) {

                    Job job = Job.builder().jobId(resultSet.getString("job_id")).build();
                    Employee manager = Employee.builder().employeeId(resultSet.getInt("manager_id") == 0 ? null : resultSet.getInt("manager_id")).build();
                    Department department = Department.builder().departmentId(resultSet.getInt("department_id") == 0 ? null : resultSet.getInt("department_id")).build();
                    employee = Employee.builder()
                            .employeeId(resultSet.getInt("employee_id"))
                            .firstName(resultSet.getString("first_name"))
                            .lastName(resultSet.getString("last_name"))
                            .email(resultSet.getString("email"))
                            .phoneNumber(resultSet.getString("phone_number"))
                            .hireDate(resultSet.getDate("hire_date"))
                            .job(job)
                            .salary(resultSet.getBigDecimal("salary"))
                            .commissionPct(resultSet.getBigDecimal("commission_pct"))
                            .manager(manager)
                            .department(department)
                            .build();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(employee);
    }

    @Override
    public Employee save(Employee employee) {
        if (employee.getEmployeeId() == null) {
            return insertRow(employee);
        }

        return updateRow(employee);
    }


    @Override
    public void deleteById(Integer id) {
        String sql = "DELETE FROM employees WHERE employee_id = ?";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);) {

            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void freeManager(Integer id) {
        String sql = "UPDATE employees SET manager_id = NULL WHERE manager_id = ?";
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {

                stmt.setInt(1, id);

                stmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void freeDepartmentFromManager(Integer id) {
        String sql = "UPDATE departments SET manager_id = NULL WHERE manager_id = ?";
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setInt(1, id);
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Integer findNextPrimaryKeyValue() {
        String sql = "SELECT employees_seq.nextval FROM dual";
        Integer id = null;
        try {

            try (Connection conn = dataSource.getConnection();
                 Statement stmt = conn.createStatement()) {
                stmt.execute(sql);

                ResultSet resultSet = stmt.getResultSet();
                resultSet.next();
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    private Employee insertRow(Employee employee) {
        String sql = "INSERT INTO employees(" +
                "employee_id,first_name, last_name, email, phone_number, hire_date, " +
                "job_id, salary, commission_pct,manager_id, department_id)" +
                "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {

            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {
                Integer id = findNextPrimaryKeyValue();
                stmt.setInt(1, id);
                stmt.setString(2, employee.getFirstName());
                stmt.setString(3, employee.getLastName());
                stmt.setString(4, employee.getEmail());
                stmt.setString(5, employee.getPhoneNumber());
                stmt.setDate(6, employee.getHireDate());
                stmt.setString(7, employee.getJob() == null ? null : employee.getJob().getJobId());
                stmt.setBigDecimal(8, employee.getSalary());
                stmt.setBigDecimal(9, employee.getCommissionPct());
                stmt.setObject(10, employee.getManager() == null ? null : employee.getManager().getEmployeeId());
                stmt.setObject(11, employee.getDepartment() == null ? null : employee.getDepartment().getDepartmentId());

                stmt.executeUpdate();
                employee.setEmployeeId(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    private Employee updateRow(Employee employee) {
        String sql = "UPDATE employees " +
                " SET first_name = ? , last_name = ? , email = ? , phone_number = ? , hire_date = ? , " +
                "job_id = ? , salary = ? , commission_pct = ? ,manager_id = ? , department_id = ? " +
                " WHERE employee_id = ?";
        try {

            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {
                stmt.setString(1, employee.getFirstName());
                stmt.setString(2, employee.getLastName());
                stmt.setString(3, employee.getEmail());
                stmt.setString(4, employee.getPhoneNumber());
                stmt.setDate(5, employee.getHireDate());
                System.out.println(employee.getJob() == null);
                stmt.setString(6, employee.getJob() == null ? null : employee.getJob().getJobId());
                stmt.setBigDecimal(7, employee.getSalary());
                stmt.setBigDecimal(8, employee.getCommissionPct());
                stmt.setObject(9, employee.getManager() == null ? null : employee.getManager().getEmployeeId());
                stmt.setObject(10, employee.getDepartment() == null ? null : employee.getDepartment().getDepartmentId());
                stmt.setInt(11, employee.getEmployeeId());
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return employee;
    }
}
