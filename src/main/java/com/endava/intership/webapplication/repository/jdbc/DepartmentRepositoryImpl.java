package com.endava.intership.webapplication.repository.jdbc;

import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Location;
import com.endava.intership.webapplication.repository.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Profile("jdbc")
@RequiredArgsConstructor
@Repository
public class DepartmentRepositoryImpl implements DepartmentRepository {

    private final DataSource dataSource;

    @Override
    public List<Department> findAll() {
        String sql = "SELECT * FROM departments";
        ArrayList<Department> departments = null;
        try {
            try (Connection conn = dataSource.getConnection(); Statement stmt = conn.createStatement()) {
                stmt.execute(sql);

                ResultSet resultSet = stmt.getResultSet();
                departments = new ArrayList<>();
                while (resultSet.next()) {
                    Location location = Location.builder().locationId(resultSet.getInt("location_id") == 0 ? null : resultSet.getInt("location_id")).build();
                    Employee employee = Employee.builder().employeeId(resultSet.getInt("manager_id") == 0 ? null : resultSet.getInt("manager_id")).build();
                    Department department = Department.builder()
                            .departmentId(resultSet.getInt("department_id"))
                            .departmentName(resultSet.getString("department_name"))
                            .manager(employee)
                            .location(location)
                            .build();
                    departments.add(department);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return departments;
    }

    @Override
    public Optional<Department> findById(Integer id) {
        Department department = null;
        String sql = " SELECT * FROM departments WHERE department_id = ? ";

        try {


            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {

                stmt.setInt(1, id);
                stmt.execute();

                ResultSet resultSet = stmt.getResultSet();
                if (resultSet.next()) {
                    Location location = Location.builder().locationId(resultSet.getInt("location_id") == 0 ? null : resultSet.getInt("location_id")).build();
                    Employee employee = Employee.builder().employeeId(resultSet.getInt("manager_id") == 0 ? null : resultSet.getInt("manager_id")).build();
                    department = Department.builder()
                            .departmentId(resultSet.getInt("department_id"))
                            .departmentName(resultSet.getString("department_name"))
                            .manager(employee)
                            .location(location)
                            .build();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.ofNullable(department);
    }

    @Override
    public Department save(Department department) {
        if (department.getDepartmentId() == null) {
            return insertRow(department);
        }

        return updateRow(department);

    }


    @Override
    public void deleteById(Integer id) {
        String sql = "DELETE FROM departments WHERE department_id = ?";
        try (Connection conn = dataSource.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void freeEmployeesFromDepartment(Integer id) {
        String sql = "UPDATE employees SET department_id = NULL WHERE department_id = ?";
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {

                stmt.setInt(1, id);
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Integer findNextPrimaryKeyValue() {
        String sql = "SELECT departments_seq.nextval FROM dual";
        Integer id = null;
        try {

            try (Connection conn = dataSource.getConnection();
                 Statement stmt = conn.createStatement()) {
                stmt.execute(sql);

                ResultSet resultSet = stmt.getResultSet();
                resultSet.next();
                id = resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    private Department insertRow(Department department) {
        String sql = "INSERT INTO departments(" +
                "department_id,department_name  , manager_id , location_id)  " +
                "VALUES (?,?, ?, ?)";
        try {

            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {
                Integer id = findNextPrimaryKeyValue();
                stmt.setInt(1, id);
                stmt.setString(2, department.getDepartmentName());
                stmt.setObject(3, department.getManager() == null ? 0 : department.getManager().getEmployeeId());
                stmt.setObject(4, department.getLocation() == null ? 0 : department.getLocation().getLocationId());

                stmt.executeUpdate();
                department.setDepartmentId(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return department;
    }

    private Department updateRow(Department department) {
        String sql = "UPDATE departments SET " +
                "department_name = ? , manager_id = ? , location_id = ? " +
                "WHERE department_id = ? ";
        try {

            try (Connection conn = dataSource.getConnection();
                 PreparedStatement stmt = conn.prepareStatement(sql)) {


                stmt.setString(1, department.getDepartmentName());
                stmt.setObject(2, department.getManager() == null ? 0 : department.getManager().getEmployeeId());
                stmt.setObject(3, department.getLocation() == null ? 0 : department.getLocation().getLocationId());
                stmt.setInt(4, department.getDepartmentId());
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return department;
    }

}
