package com.endava.intership.webapplication.repository.data;

import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.repository.DepartmentRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

@Profile("data")
public interface DepartmentRepositorySpringData extends JpaRepository<Department, Integer>, DepartmentRepository {

    @Modifying
    @Query(value = " UPDATE employees SET department_id = NULL WHERE department_id = ?1",
            nativeQuery = true)
    void freeEmployeesFromDepartment(Integer departmentId);
}
