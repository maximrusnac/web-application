package com.endava.intership.webapplication.repository.hibernate;

import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.repository.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Profile("hibernate")
@RequiredArgsConstructor
@Repository
public class DepartmentRepositoryHibernateImpl implements DepartmentRepository {
    private final SessionFactory sessionFactory;

    @Override
    public List<Department> findAll() {
        List<Department> departments = null;
        try (Session session = sessionFactory.openSession()) {
            departments = session.createQuery("from Department", Department.class).list();
        }

        return departments;
    }

    @Override
    public Optional<Department> findById(Integer id) {
        Department department = null;
        try (Session session = sessionFactory.openSession()) {
            Query<Department> query = session.createQuery("from Department WHERE departmentId = :id", Department.class);
            query.setParameter("id", id);
            query.setMaxResults(1);
            department = query.uniqueResult();
        }

        return Optional.ofNullable(department);
    }

    @Transactional
    @Override
    public Department save(Department department) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(department);
            transaction.commit();
        }
        return department;
    }


    @Override
    public void deleteById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query query = session.createQuery("delete Department where departmentId = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        }
    }

    @Override
    public void freeEmployeesFromDepartment(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query query = session.createSQLQuery("UPDATE employees SET department_id = NULL WHERE department_id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        }
    }
}
