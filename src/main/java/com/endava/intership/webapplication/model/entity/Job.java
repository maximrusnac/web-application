package com.endava.intership.webapplication.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "jobs")
public class Job {
    @Id
    @Column(name = "job_id")
    String jobId;

    @Column(name = "job_title")
    String jobTitle;

    @Column(name = "min_salary")
    Integer minSalary;

    @Column(name = "max_salary")
    Integer maxSalary;
}
