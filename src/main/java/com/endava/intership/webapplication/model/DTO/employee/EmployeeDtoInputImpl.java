package com.endava.intership.webapplication.model.DTO.employee;

import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Job;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.sql.Date;

@Data
public class EmployeeDtoInputImpl implements EmployeeDto {
    @NotBlank(message = "Can not be blank, empty or null")
    String firstName;
    @NotBlank(message = "Can not be blank, empty or null")
    String lastName;
    @Email(message = "Should contain @")
    String email;
    @Pattern(regexp = "0[\\d]{8}$", message = "Number should start with 0 and length should be 9(includes first 0)")
    String phoneNumber;
    Date hireDate;
    Job job;
    @Min(value = 1, message = "Minimum salary is 1")
    Float salary;
    Float commissionPct;
    Employee manager;
    Department department;
}
