package com.endava.intership.webapplication.model.DTO.department;

import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.model.entity.Location;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DepartmentDtoInputImpl implements DepartmentDto {
    @NotBlank(message = "Can not be blank, empty or null")
    String departmentName;
    Employee manager;
    Location location;
}
