package com.endava.intership.webapplication.model.DTO.employee;

import lombok.Data;

@Data
public class EmployeeDtoLimitedFieldsImpl implements EmployeeDto {
    String firstName;
    String lastName;
    Integer departmentId;

}
