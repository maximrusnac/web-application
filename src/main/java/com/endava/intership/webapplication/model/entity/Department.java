package com.endava.intership.webapplication.model.entity;

import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoInputImpl;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "departments")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Department {
    @Id
    @Column(name = "department_id")
    @SequenceGenerator(name = "dep_seq", sequenceName = "DEPARTMENTS_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "dep_seq", strategy = GenerationType.SEQUENCE)
    private Integer departmentId;

    @Column(name = "department_name")
    private String departmentName;


    @ManyToOne(fetch=FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name = "manager_id")
    private Employee manager;

    @ManyToOne(fetch=FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name = "location_id")
    private Location location;
}
