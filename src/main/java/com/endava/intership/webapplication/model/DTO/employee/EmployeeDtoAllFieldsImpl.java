package com.endava.intership.webapplication.model.DTO.employee;

import lombok.Data;

import java.sql.Date;

@Data
public class EmployeeDtoAllFieldsImpl implements EmployeeDto {
    private Integer employeeId;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private Date hireDate;

    private String jobId;

    private Float salary;

    private Float commissionPct;

    private Integer managerId;

    private Integer departmentId;

}
