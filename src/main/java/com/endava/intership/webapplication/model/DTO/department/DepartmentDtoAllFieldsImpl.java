package com.endava.intership.webapplication.model.DTO.department;


import lombok.Data;

@Data
public class DepartmentDtoAllFieldsImpl implements DepartmentDto {
    private Integer departmentId;

    private String departmentName;

    private Integer managerId;

    private Integer locationId;

}
