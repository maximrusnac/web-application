package com.endava.intership.webapplication.model.DTO.department;

import lombok.Data;


@Data
public class DepartmentDtoLimitedFieldsImpl implements DepartmentDto {
    String departmentName;

}
