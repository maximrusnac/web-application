package com.endava.intership.webapplication.controller;

import com.endava.intership.webapplication.model.DTO.department.DepartmentDto;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoInputImpl;
import com.endava.intership.webapplication.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/departments/")
public class DepartmentController {

    private final DepartmentService departmentServiceImpl;

    @GetMapping
    public ResponseEntity<List<DepartmentDto>> findAllDepartments(@RequestParam(defaultValue = "false") Boolean expand) {

        return new ResponseEntity<>(departmentServiceImpl.findAllDepartments(expand), HttpStatus.FOUND);
    }

    @GetMapping("{id}")
    public ResponseEntity<DepartmentDto> findByIdDepartment(@PathVariable Integer id, @RequestParam(defaultValue = "false") Boolean expand) {
        return new ResponseEntity<>(departmentServiceImpl.findByIdDepartment(id, expand), HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity<DepartmentDto> saveDepartment(@RequestBody @Valid DepartmentDtoInputImpl departmentDtoInputImpl, @RequestParam(defaultValue = "false") Boolean expand) {

        return new ResponseEntity<>(departmentServiceImpl.saveDepartment(departmentDtoInputImpl, expand), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<DepartmentDto> updateDepartment(@PathVariable Integer id, @RequestBody @Valid DepartmentDtoInputImpl departmentDtoInputImpl, @RequestParam(defaultValue = "false") Boolean expand) {

        return new ResponseEntity<>(departmentServiceImpl.updateDepartment(id, departmentDtoInputImpl, expand), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteDepartment(@PathVariable Integer id) {
        departmentServiceImpl.deleteDepartment(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
