package com.endava.intership.webapplication.controller;

import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoInputImpl;
import com.endava.intership.webapplication.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/employees/")
public class EmployeeController {

    private final EmployeeService employeeServiceImpl;

    @GetMapping
    public ResponseEntity<List<EmployeeDto>> findAllEmployees(@RequestParam(defaultValue = "false") Boolean expand) {
        return new ResponseEntity<>(employeeServiceImpl.findAllEmployees(expand), HttpStatus.FOUND);
    }

    @GetMapping("{id}")
    public ResponseEntity<EmployeeDto> findByIdEmployee(@PathVariable Integer id, @RequestParam(defaultValue = "false") Boolean expand) {

        return new ResponseEntity<>(employeeServiceImpl.findByIdEmployee(id, expand), HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity<EmployeeDto> saveEmployee(@RequestBody @Valid EmployeeDtoInputImpl employeeDtoInputImpl, @RequestParam(defaultValue = "false") Boolean expand) {
        return new ResponseEntity<>(
                employeeServiceImpl.saveEmployee(employeeDtoInputImpl, expand), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<EmployeeDto> updateEmployee(@PathVariable Integer id, @RequestBody @Valid EmployeeDtoInputImpl employeeDtoInputImpl, @RequestParam(defaultValue = "false") Boolean expand) {
        return new ResponseEntity<>(employeeServiceImpl.updateEmployee(id, employeeDtoInputImpl, expand), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable Integer id) {
        employeeServiceImpl.deleteEmployee(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
