package com.endava.intership.webapplication.service;

import com.endava.intership.webapplication.model.DTO.department.DepartmentDto;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoInputImpl;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoLimitedFieldsImpl;
import com.endava.intership.webapplication.model.entity.Department;
import com.endava.intership.webapplication.repository.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepositoryImpl;
    private final ModelMapper modelMapper;

    @Override
    public List<DepartmentDto> findAllDepartments(Boolean expand) {
        List<Department> departments = departmentRepositoryImpl.findAll();
        if (expand) {
            return departments
                    .stream()
                    .map(this::convertToAllFieldsDto)
                    .collect(Collectors.toList());
        } else {
            return departments
                    .stream()
                    .map(this::convertToLimitedFieldsDto)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public DepartmentDto findByIdDepartment(Integer id, Boolean expand) {
        Department department = departmentRepositoryImpl.findById(id).orElseThrow(NoSuchElementException::new);
        if (expand) {
            return convertToAllFieldsDto(department);
        } else {
            return convertToLimitedFieldsDto(department);
        }
    }

    @Transactional
    @Override
    public DepartmentDto saveDepartment(DepartmentDtoInputImpl departmentDtoInputImpl, Boolean expand) {
        Department department = convertToDepartment(departmentDtoInputImpl);
        department = departmentRepositoryImpl.save(department);
        if (expand) {
            return convertToAllFieldsDto(department);
        } else {
            return convertToLimitedFieldsDto(department);
        }
    }

    @Transactional
    @Override
    public void deleteDepartment(Integer id) {
        departmentRepositoryImpl.freeEmployeesFromDepartment(id);
        departmentRepositoryImpl.deleteById(id);
    }

    @Transactional
    @Override
    public DepartmentDto updateDepartment(Integer id, DepartmentDtoInputImpl departmentDtoInputImpl, Boolean expand) {
        departmentRepositoryImpl.findById(id).orElseThrow(NoSuchElementException::new);
        Department department = convertToDepartment(departmentDtoInputImpl);
        department.setDepartmentId(id);
        Department departmentAfterSave = departmentRepositoryImpl.save(department);

        if (expand) {
            return convertToAllFieldsDto(departmentAfterSave);
        } else {
            return convertToLimitedFieldsDto(departmentAfterSave);
        }
    }

    private DepartmentDtoAllFieldsImpl convertToAllFieldsDto(Department department) {
        return modelMapper.map(department, DepartmentDtoAllFieldsImpl.class);
    }

    private DepartmentDtoLimitedFieldsImpl convertToLimitedFieldsDto(Department department) {
        return modelMapper.map(department, DepartmentDtoLimitedFieldsImpl.class);
    }

    private Department convertToDepartment(DepartmentDto department) {
        return modelMapper.map(department, Department.class);
    }


}
