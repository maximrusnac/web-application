package com.endava.intership.webapplication.service;

import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoInputImpl;

import java.util.List;

public interface EmployeeService {
    List<EmployeeDto> findAllEmployees(Boolean expand);

    EmployeeDto findByIdEmployee(Integer id, Boolean expand);

    EmployeeDto saveEmployee(EmployeeDtoInputImpl employeeDtoInputImpl, Boolean expand);

    void deleteEmployee(Integer id);

    EmployeeDto updateEmployee(Integer id, EmployeeDtoInputImpl employeeDtoInputImpl, Boolean expand);

}
