package com.endava.intership.webapplication.service;

import com.endava.intership.webapplication.model.DTO.department.DepartmentDto;
import com.endava.intership.webapplication.model.DTO.department.DepartmentDtoInputImpl;

import java.util.List;

public interface DepartmentService {
    List<DepartmentDto> findAllDepartments(Boolean expand);

    DepartmentDto findByIdDepartment(Integer id, Boolean expand);

    DepartmentDto saveDepartment(DepartmentDtoInputImpl departmentDtoInputImpl, Boolean expand);

    void deleteDepartment(Integer id);

    DepartmentDto updateDepartment(Integer id, DepartmentDtoInputImpl departmentDtoInputImpl, Boolean expand);
}
