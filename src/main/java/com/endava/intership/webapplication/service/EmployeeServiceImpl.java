package com.endava.intership.webapplication.service;

import com.endava.intership.webapplication.model.DTO.employee.EmployeeDto;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoAllFieldsImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoInputImpl;
import com.endava.intership.webapplication.model.DTO.employee.EmployeeDtoLimitedFieldsImpl;
import com.endava.intership.webapplication.model.entity.Employee;
import com.endava.intership.webapplication.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepositoryImpl;
    private final ModelMapper modelMapper;

    @Override
    public List<EmployeeDto> findAllEmployees(Boolean expand) {

        List<Employee> employees = employeeRepositoryImpl.findAll();
        if (expand) {
            return employees
                    .stream()
                    .map(this::convertToAllFields)
                    .collect(Collectors.toList());
        } else {
            return employees
                    .stream()
                    .map(this::convertToLimitedFields)
                    .collect(Collectors.toList());
        }
    }

    @Override
    public EmployeeDto findByIdEmployee(Integer id, Boolean expand) {
        Employee employee = employeeRepositoryImpl.findById(id).orElseThrow(NoSuchElementException::new);

        if (expand) {
            return convertToAllFields(employee);
        } else {
            return convertToLimitedFields(employee);
        }
    }

    @Transactional
    @Override
    public EmployeeDto saveEmployee(EmployeeDtoInputImpl employeeDtoInputImpl, Boolean expand) {
        Employee employee = convertToEmployee(employeeDtoInputImpl);
        employee = employeeRepositoryImpl.save(employee);
        if (expand) {
            return convertToAllFields(employee);
        } else {
            return convertToLimitedFields(employee);
        }
    }

    @Transactional
    @Override
    public void deleteEmployee(Integer id) {
        employeeRepositoryImpl.freeManager(id);
        employeeRepositoryImpl.freeDepartmentFromManager(id);
        employeeRepositoryImpl.deleteById(id);
    }

    @Transactional
    @Override
    public EmployeeDto updateEmployee(Integer id, EmployeeDtoInputImpl employeeDtoInputImpl, Boolean expand) {
        employeeRepositoryImpl.findById(id).orElseThrow(NoSuchElementException::new);
        Employee employee = convertToEmployee(employeeDtoInputImpl);
        employee.setEmployeeId(id);
        Employee employeeAfterSave = employeeRepositoryImpl.save(employee);
        if (expand) {
            return convertToAllFields(employeeAfterSave);
        } else {
            return convertToLimitedFields(employeeAfterSave);
        }
    }

    private EmployeeDtoAllFieldsImpl convertToAllFields(Employee employee) {
        return modelMapper.map(employee, EmployeeDtoAllFieldsImpl.class);
    }

    private EmployeeDtoLimitedFieldsImpl convertToLimitedFields(Employee employee) {
        return modelMapper.map(employee, EmployeeDtoLimitedFieldsImpl.class);
    }

    private Employee convertToEmployee(EmployeeDto employeeDto) {
        return modelMapper.map(employeeDto, Employee.class);
    }
}
